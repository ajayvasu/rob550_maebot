from scipy import ndimage
import numpy as np
import scipy.ndimage.filters as filters
import scipy.ndimage.morphology as morphology
from PIL import Image
map_dir = '/home/student/team12/rob550_maebot/src/botlab/map_images/'
thresh = 200
map_fname = map_dir+'2015-10-09T15h26m19s_8meters.npy'
map_data = np.load(map_fname)
#Image.fromarray(np.uint8(map_data)).save(map_dir+'img.png')
img = map_data
#footprint=np.ones((4,4))
img = filters.minimum_filter(map_data,10)
#img = img>thresh

img = np.where(img < thresh, 0, 255)
#blurred_l = ndimage.gaussian_filter(map_data, 5)
final_img = img
Image.fromarray(np.uint8(final_img)).save(map_dir+'proc_img.png')
np.save(map_dir+'proc_img', final_img)
