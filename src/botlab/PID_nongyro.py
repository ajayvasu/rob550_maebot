# PID.py
# Former one: distance as the x, d_dist is the errordot
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import division
import sys, os, time,signal
import lcm
import math 
import PID

from lcmtypes import maebot_diff_drive_t
from lcmtypes import maebot_motor_feedback_t
from lcmtypes import velocity_cmd_t
from lcmtypes import pid_init_t
from math import *
from collections import deque

class PID():
    def __init__(self, kp, ki, kd):
        # Main Gains
        self.kp = kp
        self.ki = ki
        self.kd = kd
            
        # Integral Terms
        self.iTerm = 0.0
        self.iTermMin = -0.0
        self.iTermMax = 0.0

        # Input (feedback signal)
        self.prevInput = 0.0
        self.input = 0.0

        # Reference Signals (signal and derivative!)
        self.dotsetpoint = 0.0          
        self.setpoint = 0.0

        # Output signal and limits
        self.output = 0.0
        self.outputMin = -0.35
        self.outputMax = 0.35

        # Update Rate
        self.updateRate = 0.05

        # Error Signals
        self.error = 0.0
        self.errordot = 0.0
        self.errorList = deque([0]*20)
        self.prop_term = 0.0
        self.der_term = 0.0
        self.int_term = 0.0
        
    def Compute(self):
        # IMPLEMENT ME!
        # Different then last project! 
        # Now you have a speed reference also!
        # Also Update Rate can be gone here or in SetTunnings function
        # (it is your choice!) But change functions consistently  
        
        output = 0.0
        self.error = self.setpoint - self.input
        self.errordot = self.dotsetpoint - (self.input-self.prevInput)
        # Calculate the Integral Term
        self.errorList.append(self.error)
        self.errorList.popleft()
        self.iTerm = sum(self.errorList)

        print("uncliped iTerm: "+ str(self.iTerm))

        # Clip Integral Term
        self.iTerm = max(self.iTermMin,min(self.iTerm, self.iTermMax))
        
        # Calculate the three terms
        self.prop_term = self.kp* self.error
        self.der_term = self.kd* self.errordot
        self.int_term = self.ki* self.iTerm

        # Sum all terms
        output = self.int_term+self.prop_term+self.der_term
        # Clip output
        output = max(self.outputMin,min(self.outputMax,output))
        #print("iTerm: "+ str(self.iTerm))
        print("Gains -" +str(self.kp)+" , "+str(self.ki)+" , "+str(self.kd))
        #print("Errors - "+str(self.error)+" , "+str(self.errordot))
        return output
    
    # Accessory Function to Change Gains
    def SetTunings(self, kp, ki, kd):
        self.kp = kp * self.updateRate
        self.ki = ki * self.updateRate
        self.kd = kd * self.updateRate

    def SetIntegralLimits(self, imin, imax):
        self.iTermMin = imin
        self.iTermMax = imax

    def SetOutputLimits(self, outmin, outmax):
        self.outputMin = outmin
        self.outputMax = outmax
                
    def SetUpdateRate(self, rateInSec):
        self.updateRate = rateInSec



class PIDController():
    def __init__(self):
     
        # Create Both PID
        self.leftCtrl =  PID(0.0,0.0,0.0)
        self.rightCtrl = PID(0.0,0.0,0.0)
 
        # LCM Subscribe
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", 
                                        self.motorFeedbackHandler)
        lcmCommandSub = self.lc.subscribe("GS_VELOCITY_CMD", 
                                        self.motorCommandHandler)
        lcmCommandSub = self.lc.subscribe("GS_PID_INIT", 
                                        self.PIDInitHandler)
        signal.signal(signal.SIGINT, self.signal_handler)


        # Odometry 
        self.wheelDiameterMillimeters = 32.0  # diameter of wheels [mm]
        self.axleLengthMillimeters = 80.0     # separation of wheels [mm]    
        self.ticksPerRev = 16.0           # encoder tickers per motor revolution
        self.gearRatio = 30.0             # 30:1 gear ratio
        self.enc2mm = ((math.pi * self.wheelDiameterMillimeters) / 
                       (self.gearRatio * self.ticksPerRev)) 
                        # encoder ticks to distance [mm]

        self.prevReadEncTime = time.time()
        self.encReady = False
        self.refReady = False
        # Initialization Variables
        #self.InitialPosition = (0.0, 0.0)
        #self.oldTime = 0.0
        self.startTime = 0.0
        self.totalTime = 0.0
        self.ref_right_totalTime = 0.0
        self.ref_left_totalTime = 0.0
        self.ref_right_dist = 0.0
        self.ref_left_dist = 0.0
        self.actual_right_dist = 0.0
        self.actual_left_dist = 0.0

    def publishMotorCmd(self):
        cmd = maebot_diff_drive_t()
        
        #print "Publishing Diff Drive"
        print("Prop term: "+str(self.rightCtrl.prop_term)+" , "+str(self.leftCtrl.prop_term))
        print("Deri term: "+str(self.rightCtrl.der_term)+" , "+str(self.leftCtrl.der_term))
        print("Int  term: "+str(self.rightCtrl.int_term)+" , "+str(self.leftCtrl.int_term))
        if (self.actual_left_dist < self.leftCtrl.setpoint - 100):
            cmd.motor_left_speed = self.leftCtrl.Compute() + 0.055
        elif (self.actual_left_dist >self.leftCtrl.setpoint + 100):
            cmd.motor_left_speed = self.leftCtrl.Compute() - 0.055
        
        if (self.actual_right_dist < self.rightCtrl.setpoint - 100):
            cmd.motor_right_speed = self.rightCtrl.Compute() + 0.055
        elif (self.actual_right_dist > self.rightCtrl.setpoint +100):
            cmd.motor_right_speed = self.rightCtrl.Compute() - 0.055
        
        #self.lc.publish("TEST_DIFF_DRIVE",cmd.encode())
        self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())

    def motorFeedbackHandler(self,channel,data):
        msg = maebot_motor_feedback_t.decode(data)

        self.currEncPos = (msg.encoder_left_ticks, msg.encoder_right_ticks, 0.0)

        time_now = time.time()
        
        if (not self.encReady):
            self.encReady = True
            self.initTicks = self.currEncPos
            self.prevEncPos = self.currEncPos
            self.startTime = time.time()
        # Compute time
        #self.dtEnc = time_now - self.prevReadEncTime
        #self.prevReadEncTime = time_now
        self.totalTime = time_now - self.startTime

        # Actual Ticks per second
        #self.actual_left_dot_dist = (self.currEncPos[0] - self.prevEncPos[0])/dtEnc
        #self.actual_right_dot_dist = (self.currEncPos[1] - self.prevEncPos[1])dtEnc
        
        self.prevEncPos = self.currEncPos
        
        # Actual Total Ticks traversed
        if (self.encReady):
            self.actual_left_dist = self.currEncPos[0] - self.initTicks[0]
            self.actual_right_dist = self.currEncPos[1] - self.initTicks[1]
        
        # Set the paramters to the pid class
        if (self.encReady and self.refReady):
            # Set the input to the pid class
            self.leftCtrl.input = self.actual_left_dist
            self.rightCtrl.input = self.actual_right_dist
            
            # Set the setpoints and the setpoints of the pid
            if ((self.ref_left_dist - self.actual_left_dist)/self.ref_left_vel > 0):
                self.leftCtrl.setpoint = self.totalTime * self.ref_left_vel
                
            else:
                self.leftCtrl.setpoint = self.ref_left_dist
                self.leftCtrl.dotsetpoint = 0
                
            if ((self.ref_right_dist - self.actual_right_dist)/self.ref_right_vel > 0):
                self.rightCtrl.setpoint = self.ref_right_vel * self.totalTime
            else:
                self.rightCtrl.setpoint = self.ref_right_dist
                self.rightCtrl.dotsetpoint = 0


            # Compute velocity error(errordot)
            
            #if (self.totalTime <= self.ref_right_totalTime):
            #    self.rightCtrl.errordot = self.ref_right_vel - self.actual_right_vel
            #else:
            #    self.rightCtrl.errordot = -self.actual_right_vel

            #if (self.totalTime <= self.ref_left_totalTime):
            #    self.leftCtrl.errordot = self.ref_left_vel - self.actual_left_vel
            #else:
            #    self.leftCtrl.errordot = -self.actual_left_vel
            
            print("Total T:   "+str(self.totalTime))
            

    def motorCommandHandler(self,channel,data):
        msg = velocity_cmd_t.decode(data)
        # IMPLEMENT ME
        # Change to Receive the Message from Ground Station
        # And command the bot
        msg = velocity_cmd_t.decode(data)
        ang_speed  = msg.AngSpeed*pi/180.0
        
        # Initialize the PID for a new command
        self.rightCtrl.error = 0.0
        self.leftCtrl.error = 0.0
        self.rightCtrl.errordot = 0.0
        self.leftCtrl.errordot = 0.0
        self.rightCtrl.errorList = deque([0]*20)
        self.leftCtrl.errorList = deque([0]*20)
        self.totalTime = 0.0
        
        # Velocity
        vr = msg.FwdSpeed + ang_speed*self.axleLengthMillimeters/2
        vl = msg.FwdSpeed - ang_speed*self.axleLengthMillimeters/2

        # Reference Ticks per second
        self.ref_left_vel = vl/self.enc2mm	
        self.ref_right_vel = vr/self.enc2mm	

        msg.Angle = msg.Angle*pi/180.0
        # Distance

        # Should we use the updating distance??????
        dr = msg.Distance + msg.Angle*self.axleLengthMillimeters/2
        dl = msg.Distance - msg.Angle*self.axleLengthMillimeters/2

        self.ref_left_totalTime = dl/vl
        self.ref_right_totalTime = dr/vr
        
        self.ref_left_dist = dl/self.enc2mm
        self.ref_right_dist = dr/self.enc2mm
        
        self.leftCtrl.setpoint = 0
        self.rightCtrl.setpoint = 0
        self.leftCtrl.dotsetpoint = self.ref_left_vel * self.leftCtrl.updateRate
        self.rightCtrl.dotsetpoint = self.ref_right_vel * self.rightCtrl.updateRate
        self.refReady = True
        self.encReady = False
    
    def PIDInitHandler(self,channel,data):
        msg = pid_init_t.decode(data)
        self.rightCtrl.SetTunings(msg.kp,msg.ki,msg.kd)
        self.rightCtrl.SetIntegralLimits(-msg.iSat,msg.iSat)
        self.leftCtrl.SetTunings(msg.kp,msg.ki,msg.kd)
        self.leftCtrl.SetIntegralLimits(-msg.iSat,msg.iSat)

    def Controller(self):

        # For now give a fixed command here
        # Later code to get from groundstation should be used
        #DesiredSpeed = 150.0
        #DesiredDistance = 1000.0
        last_time = time.time()
        while(1):
            time_now = time.time()
            self.lc.handle()
            # IMPLEMENT ME
            if(time_now - last_time > self.rightCtrl.updateRate):
                last_time = time_now
                self.publishMotorCmd()
    
       
    # Function to print 0 commands to morot when exiting with Ctrl+C 
    # No need to change 
    def signal_handler(self,signal, frame):
        print("Terminating!")
        for i in range(5):
            cmd = maebot_diff_drive_t()
            cmd.motor_right_speed = 0.0
            cmd.motor_left_speed = 0.0  
            self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
        exit(1)

pid = PIDController()
pid.Controller()

