from scipy import ndimage
import numpy as np
import scipy.ndimage.filters as filters
import scipy.ndimage.morphology as morphology
from PIL import Image, ImageDraw
import sys
import math
import time
from scipy import signal

SIG_TRAPPED = 0
SIG_ADVANCED = 1
SIG_REACHED = 2
SIG_SUCCESS  = 10


def dist2d(p1,p2):
    p = (p1[0]-p2[0],p1[1]-p2[1])
    sqdist = p[0]*p[0]+p[1]*p[1]
    return math.sqrt(sqdist)


# For tuples
class RRT():
    def __init__(self,img,init,eps=3,max_nodes=500,name="unnamed",thickness = 5):
        self.epsilon = eps
        self.name = name
        self.max_nodes = max_nodes
        self.tree_nodes = [init]
        self.min_obs_thickness = thickness
        # Index into tree_nodes
        self.tree_parents = [0]
        self.tree_numnodes = 1
        # Dark regions are not valid --  a binary image

        self.img = img
        # Keep a random array instead of sampling each time.
        self.rand_arr_x = np.random.rand(max_nodes).tolist()
        self.rand_arr_y = np.random.rand(max_nodes).tolist()


    # TODO:Change to a KDTree with a buffer list once list crosses 500 nodes
    def nn(self,q):
        closest = 9999999
        nnode = []
        closest_idx = 999999
        for idx in xrange(self.tree_numnodes):
            node = self.tree_nodes[idx]
            dist = dist2d(q,node)
            if dist<closest:
                closest = dist
                nnode = node
                closest_node = idx
        return closest_node,nnode

    def step(self,q,q_target):
        if dist2d(q,q_target) < self.epsilon:
            return q_target
        else:
            theta = math.atan2(q_target[1]-q[1],q_target[0]-q[0])
            return (q[0] + self.epsilon*math.cos(theta), q[1] + self.epsilon*math.sin(theta))

    def is_valid(self,q):
	#print q
        x,y  = int(q[0]),int(q[1])
        #print "At ({0},{1}), value = {val}".format(x,y,val=img[x,y])
        if(self.img[x,y]>0):
            return True
        return False

    def is_valid_line(self,q,q_target):
        dist = dist2d(q,q_target)
        theta = math.atan2(q_target[1]-q[1],q_target[0]-q[0])
        dist_covered = 0
        while dist_covered < dist:
            dist_covered = dist_covered + self.min_obs_thickness
            pt = (q[0] + dist_covered*math.cos(theta), q[1] + dist_covered*math.sin(theta))
            if not self.is_valid(pt):
                return False
        return True



    # TODO: Use only 1 call to Nearest neighbour for this function.
    # in future iters, it just keeps moving on..
    def connect(self,q):
        sig = SIG_ADVANCED
        nearest_idx,q_near = self.nn(q)
        while sig == SIG_ADVANCED:
            q_new = self.step(q_near,q)
            # Check if new point is valid
            #if(self.is_valid(q_new)):
            if(self.is_valid_line(q_near,q_new)):
                self.tree_nodes.append(q_new)
                #print "Inserting "+str(q_new)+" to parent "+str(nearest_idx)
                #print "Inserting "+str(q_new)+" to  tree"+self.name
                self.tree_parents.append(nearest_idx)
                self.tree_numnodes = self.tree_numnodes +1
                nearest_idx = self.tree_numnodes - 1 # The nearest element is the latest one!
                q_near = q_new
                if q_new == q:
                    sig = SIG_REACHED
                else:
                    sig =  SIG_ADVANCED
            else:
                sig = SIG_TRAPPED
        return sig

    def extend(self,q):
        nearest_idx,q_near = self.nn(q)
        if(q_near):
            q_new = self.step(q_near,q)
            # Check if new point is valid
            #if(self.is_valid(q_new)):
            if(self.is_valid_line(q_near,q_new)):
                self.tree_nodes.append(q_new)
                #print "Inserting "+str(q_new)+" to parent "+str(nearest_idx)
                #print "Inserting "+str(q_new)+" to  tree"+self.name
                self.tree_parents.append(nearest_idx)
                self.tree_numnodes = self.tree_numnodes +1
                if q_new == q:
                    return SIG_REACHED
                else:
                    return SIG_ADVANCED
            else:
                return SIG_TRAPPED
        else:
            sys.exit('Error: No nearest node!!')

    def build(self):
        dims = img.shape
        time_begin = time.time()
        for i in xrange(self.max_nodes):
            q_samp  = (self.rand_arr_x[i]*dims[0],self.rand_arr_y[i]*dims[1])
            self.extend(q_samp)
        time_end = time.time()
        #print 'Total time = {t}'.format(t=time_end-time_begin)

    # Get path from last node added
    def getPath(self):
        node = self.tree_nodes[-1]
        path = [node]
        node_idx = self.tree_numnodes - 1
        parent_node_idx = self.tree_parents[node_idx]
        while node_idx!=parent_node_idx:
            node = self.tree_nodes[parent_node_idx]
            node_idx = self.tree_parents[node_idx]
            parent_node_idx = self.tree_parents[node_idx]
            path.append(node)
        return path

    # Get sparsified path based on line of sight
    # A node will try and connect to the last node in the RRT path that
    # it can see.
    def sparsifyPath(self,path):
        prev_n = path[0]
        sparse_path = [prev_n]
        # start from idx=1
        idx = 1;
        while(1):
            curr_n = path[idx]
            while(self.is_valid_line(curr_n,prev_n)):
                if(idx == len(path)-1):
                    # We have reached the goal
                    sparse_path.append(curr_n)
                    return sparse_path
                idx = idx+1
                curr_n = path[idx]
                            # Append the last one in line of sight
            prev_n = path[idx-1]
            sparse_path.append(prev_n)
        return sparse_path


class ConnectRRT():
    def __init__(self,img,start,goal,eps=15,max_nodes=50000):
        self.start_rrt = RRT(img,start,eps,max_nodes,'A')
        self.goal_rrt = RRT(img,goal,eps,max_nodes,'B')
	self.img = img
	#xx = Image.fromarray(np.uint8(self.img)).save('/home/student/team12/new_code/src/botlab/map_images/rrt_map.png')
        self.max_nodes = max_nodes
        #Keep a random array instead of sampling each time.
        self.rand_arr_x = np.random.rand(max_nodes).tolist()
        self.rand_arr_y = np.random.rand(max_nodes).tolist()


    def plan(self):
        start = True
        dim_x,dim_y = self.img.shape
        tree_a = self.start_rrt
        tree_b = self.goal_rrt
        time_begin = time.time()
        for i in xrange(self.max_nodes):
            tree_a,tree_b = tree_b,tree_a
            start = not start
            q_samp  = (self.rand_arr_x[i]*dim_x,self.rand_arr_y[i]*dim_y)
            if not (tree_a.extend(q_samp) == SIG_TRAPPED):
                q_new = tree_a.tree_nodes[-1]
                if (tree_b.connect(q_new) == SIG_REACHED):
                    # Generate path from the trees
                    #print 'We are done!!'
                    tmp  = tree_b.sparsifyPath(tree_b.getPath())
                    tmp2 = tree_a.sparsifyPath(tree_a.getPath())
                    if start:
                        tmp2.reverse()
                        tmp2.extend(tmp[1:])
                        return tmp2
                    else:
                        tmp.reverse()
                        tmp.extend(tmp2[1:])
                        return tmp
                    time_end = time.time()
                    #print 'Total time = {t}'.format(t=time_end-time_begin)
                    #if start:
                    #    tmp2.reverse()
            #print '@ Iteration {inum}'.format(inum=i)
        time_end = time.time()
        #print 'Total time = {t}'.format(t=time_end-time_begin)

#rrt = RRT(img,(100,100))
#rrt.build()


