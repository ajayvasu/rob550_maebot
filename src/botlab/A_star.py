from scipy import ndimage
import numpy as np
import scipy.ndimage.filters as filters
import scipy.ndimage.morphology as morphology
import time
from PIL import Image
from sets import Set

map_dir = '/home/xu/rob550_maebot/src/maps/'
map_fname = map_dir+'map_data.npy'
map_data = np.load(map_fname)
print 'load done'


rowLength = map_data.shape[1]
colLength = map_data.shape[0]
numGrids = rowLength * colLength
print('row length:   ' + str(rowLength))
print('col length:   ' + str(colLength))
#grids = [0]*numGrids
#for i in range (0,numGrids):
#    if ( map_data[i / rowLength][i % rowLength] == 0):
#        grids[i] = 1
#print ('grid done')


#for i in range(0,rowLength):
#    for j in range(0,colLength):
#        f_score((i,j)) = 
f_score = [[float("inf")] * rowLength for _ in range(colLength)]
h_score = [[0] * rowLength for _ in range(colLength)]
g_score = [[float("inf")] * rowLength for _ in range(colLength)]
openset = Set()
closedset = Set()
came_from = [[-999] * rowLength for _ in range(colLength)]
start = (50,150)
goal = (250,150)



def mapCheck(pt):
    if (map_data[pt[0]][pt[1]] < 200):
        return False
    else:
        return True
        


def findNeighbours(current):
    neiSet = Set()
    if (current[0] > 0):
        nei_down = (current[0]-1,current[1])
        if ( mapCheck(nei_down)):
            neiSet.add(nei_down)
    if (current[1] > 0):
        nei_right = (current[0],current[1]-1)
        if ( mapCheck(nei_right)):
            neiSet.add(nei_right)
    if (current[1] < rowLength - 1 ):
        nei_left = (current[0],current[1]+1)
        if ( mapCheck(nei_left)):
            neiSet.add(nei_left)
    if (current[0] < colLength - 1):
        nei_up = (current[0]+1,current[1])
        if ( mapCheck(nei_up)):
            neiSet.add(nei_up)
    return neiSet

def computeHeu(start,goal):
    return (abs(start[0] - goal[0]) + abs(start[1] - goal[1]))

def mainLoop():
    startTime = time.time()
    if (mapCheck(start)):
        print ('start valid')
    else:
        return -1

    if (mapCheck(goal)):
        print ('goal valid')
    else:
        return -1

    openset.add(start)
    f_score[start[0]][start[1]] = 0
    g_score[start[0]][start[1]] = 0
    #timeSave = False
    #for i in range():
    #    h_score[i] = computeHeu(i,goal)
    for i in range(0 , rowLength):
        for j in range(0 , colLength):
            h_score[i][j] = computeHeu((i , j), goal)
    minFinOpenset = float("inf")
    #prevMinF = numGrids
    #current = nextGrid = 0
    while (len(openset) > 0):
        #if (timeSave):
        #    current = nextGrid
        #    print 'time saved'
        #    timeSave = False
        #else:
        #print openset
        minFinOpenset = float("inf")
        current = (-999,-999)
        for pt in openset:
            if (f_score[pt[0]][pt[1]] < minFinOpenset):
                minFinOpenset = f_score[pt[0]][pt[1]]
                current = pt
        #prevMinF = minFinOpenset
        #print current
        
        if current == goal:
            reconstruct_path(came_from, goal)
            totalTime = time.time() - startTime
            print ('total time:  ' + str (totalTime))
            break
        openset.remove(current)
        closedset.add(current)
        for neighbour in findNeighbours(current):
                   
            if neighbour in closedset:
                continue
            temp_g_score = g_score[current[0]][current[1]] + 1
            if (temp_g_score < g_score[neighbour[0]][neighbour[1]]):
                came_from[neighbour[0]][neighbour[1]] = current
                g_score[neighbour[0]][neighbour[1]] = temp_g_score
                f_score[neighbour[0]][neighbour[1]] = g_score[neighbour[0]][neighbour[1]] + h_score[neighbour[0]][neighbour[1]]
                #if (f_score[neighbour] == minFinOpenset):
                #    timesave = True
                #    nextGrid = neighbour
            if neighbour not in openset:
                openset.add(neighbour)


    return -1

def reconstruct_path(came_from,goal):
    totalPath = [goal]
    current = came_from[goal[0]][goal[1]]
    #while (current in came_from and current != -999):
    while (current != -999):
        prev = came_from[current[0]][current[1]]
        totalPath.append(current)
        current = prev
    #print totalPath
    for pt in totalPath:
        map_data[pt[0]][pt[1]] = 127
    
    print ('Path length: ' + str(len(totalPath)))
    final_map = map_data
    Image.fromarray(np.uint8(final_map)).save(map_dir+'path_planning.png')
    #np.save(map_dir+'map_data', final_img)
    return totalPath


mainLoop()



