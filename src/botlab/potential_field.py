from scipy import ndimage
import numpy as np
import scipy.ndimage.filters as filters
import scipy.ndimage.morphology as morphology
import time
from math import *
from PIL import Image
from sets import Set

map_dir = '/home/xu/rob550_maebot/src/maps/'
map_fname = map_dir+'map_data.npy'
map_data = np.load(map_fname)
print 'load done'

rowLength = map_data.shape[1]
colLength = map_data.shape[0]
numGrids = rowLength * colLength
print('row length:   ' + str(rowLength))
print('col length:   ' + str(colLength))

start = (50,150)
goal = (100,150)

# map() = 1 means cannot go, map() = 0 means free to go
def mapCheck(pt):
    if (map_data[pt[0]][pt[1]] < 200):
        return False
    else:
        return True

def distSquare(pt1, pt2):
    distS = pow((pt1[0] - pt2[0]),2) + pow((pt1[1] - pt2[1]),2)
    return distS

def findNeighbours(current):
    neiSet = Set()
    if (current[0] > 0):
        nei_down = (current[0]-1,current[1])
        if ( mapCheck(nei_down)):
            neiSet.add(nei_down)
    if (current[1] > 0):
        nei_right = (current[0],current[1]-1)
        if ( mapCheck(nei_right)):
            neiSet.add(nei_right)
    if (current[1] < rowLength - 1 ):
        nei_left = (current[0],current[1]+1)
        if ( mapCheck(nei_left)):
            neiSet.add(nei_left)
    if (current[0] < colLength - 1):
        nei_up = (current[0]+1,current[1])
        if ( mapCheck(nei_up)):
            neiSet.add(nei_up)
    return neiSet

# Compute the attractive field for a given point and the goal
def attractiveP(current):
    constant = 2.0
    potential = .5 * constant * distSquare(current, goal)
    return potential

# Compute the repulsive field for a give point in the map
def repulsiveP(current):
    constant = 2.0
    potential = 0.0
    effectiveDist = 5
    minDist = float("inf")
    closestPt = ()
    for i in range(-effectiveDist, effectiveDist + 1):
        for j in range(-effectiveDist, effectiveDist + 1):
            target = (current[0] + i, current[1] + j)
            if not mapCheck(target):
                distS = distSquare(target, current)
                if distS < minDist:
                    minDist = distS
                    closestPt = target
    if (minDist != float("inf")):
        potential = 0.5 * constant * pow((1.0/minDist - 1.0/effectiveDist),2)

    return potential

# Overall potential
def overallP(current):
    return attractiveP(current) - repulsiveP(current)


def mainLoop():
    startTime = time.time()
    #obstacleSet = Set()
    #freeSet = Set()
    #for i in range(0,rowLength):
    #    for j in range(0, colLength):
    #        pt = (i,j)
    #        if not mapCheck(pt):
    #            obstacleSet.add(pt)
    #        else:
    #            freeSet.add(pt)
    totalPath = [start]
    current = start
    while (current != goal):
        minPinN = float("inf")
        minNei = ()
        for neighbour in findNeighbours(current):
            neighbourP = overallP(neighbour)
            if (neighbourP < minPinN):
                minPinN = neighbourP
                minNei = neighbour
        current = minNei
        totalPath.append(current)
        print ('path length:   ' + str(len(totalPath)))
    totalTime = time.time() - startTime
    print ('total time:  ' + str(totalTime))
    print totalPath
        
mainLoop() 
