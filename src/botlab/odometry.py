# odometry.py
#
# skeleton code for University of Michigan ROB550 Botlab
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, os, time
import lcm
from math import *
from collections import deque
from breezyslam.robots import WheeledRobot

from lcmtypes import maebot_motor_feedback_t
from lcmtypes import maebot_sensor_data_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import odo_dxdtheta_t


class Maebot(WheeledRobot):

  def __init__(self):
    self.wheelDiameterMillimeters = 32.0     # diameter of wheels [mm]
    self.axleLengthMillimeters = 80.0        # separation of wheels [mm]
    self.ticksPerRev = 16.0                  # encoder tickers per motor revolution
    self.gearRatio = 30.0                    # 30:1 gear ratio
    self.gyroSensitivity = 131.0			# From datasheet

    self.enc2mm = (pi * self.wheelDiameterMillimeters) / (self.gearRatio * self.ticksPerRev) # encoder ticks to distance [mm]

    self.encReady  = False
    self.prevEncPos = (0,0,0)           # store previous readings for odometry
    self.currEncPos = (0,0,0)           # current reading for odometry
    self.prevReadTime = time.time()
    self.prevVelMesTime = time.time()
    self.prevOdoPos = (0,0,0)           # store previous x [mm], y [mm], theta [rad]
    self.currOdoPos = (0,0,0)           # store odometry x [mm], y [mm], theta [rad]
    self.currOdoVel = (0,0,0)           # store current velocity dxy [mm], dtheta [rad], dt [s]

    self.vel_xy = deque()
    self.vel_th = deque()
    self.vel_t = deque()

    self.stopped = False
    self.timeSinceStopped = -1
    self.stopTime = -1

    self.gotOneGyro = False
    self.prevGyroYaw = 0.0			# Previous integrated gyro reading for theta.
    self.currGyroYaw = 0.0			# Current integrated gyro reading for theta.

    self.gyroHistory = list()			# Stores history for bias estimation
    self.gyroReady  = False
    self.gyroBias = -999 			# Gyro bias
    self.currGyroDtheta = -1

    self.gyrodo_thresh = 0.01                    # TODO: Tune
    self.lcm_msg_counter = [0, 0]

    WheeledRobot.__init__(self, self.wheelDiameterMillimeters/2.0, self.axleLengthMillimeters/2.0)

    # LCM Initialization and Subscription
    self.lc = lcm.LCM()
    lcmMotorSub = self.lc.subscribe("MAEBOT_MOTOR_FEEDBACK", self.motorFeedbackHandler)
    lcmSensorSub = self.lc.subscribe("MAEBOT_SENSOR_DATA", self.sensorDataHandler)
    self.prevReadTime = time.time()

  def calcVelocities(self):
    # IMPLEMENT ME
    # TASK: CALCULATE VELOCITIES FOR ODOMETRY
    # Update self.currOdoVel and self.prevOdoVel
    (com_dist,dtheta) = self.getPoseChange() 
    self.currOdoVel = (com_dist, dtheta, 0.0)

    self.vel_xy.append(self.currOdoVel[0])
    self.vel_th.append(self.currOdoVel[1])
    if len(self.vel_xy) > 1:
	self.vel_xy.popleft()
	self.vel_th.popleft()

  def getVelocities(self):
    # IMPLEMENT ME
    # TASK: RETURNS VELOCITY TUPLE
    # Return a tuple of (dxy [mm], dtheta [rad], dt [s]) 

    xy_all = list(self.vel_xy)
    th_all = list(self.vel_th)
    return (sum(xy_all),sum(th_all),0.0)
  
  def getPoseChange(self):
    dleft_enc = (self.currEncPos[0] - self.prevEncPos[0]) * self.lcm_msg_counter[0]
    dright_enc = (self.currEncPos[1] - self.prevEncPos[1]) * self.lcm_msg_counter[0]

    com_dist = 0.5*self.enc2mm*(dleft_enc+dright_enc)
    curr_theta = self.currOdoPos[2]
    #Gyrodometry
    dtheta_enc = self.enc2mm*(dright_enc - dleft_enc)/self.axleLengthMillimeters
    dtheta_enc = dtheta_enc*self.lcm_msg_counter[0]
    dtheta_gyro = self.currGyroDtheta
    dtheta_gyro =  dtheta_gyro*self.lcm_msg_counter[1]
    print ("Gyro Bias = "+str(self.gyroBias))
    print("Absolute Gyrodo Diff = "+str(abs(dtheta_enc - dtheta_gyro)))
    print("Gyro = "+str( dtheta_gyro))
    print("Encoder = "+str( dtheta_enc))
    if(abs(dtheta_enc - dtheta_gyro) > self.gyrodo_thresh):
        dtheta = dtheta_gyro 
    	print("Gyro selected")
    else:
        dtheta = dtheta_enc
    	print("Encoder selected")
    return (com_dist,dtheta)


  def calcOdoPosition(self):
    # IMPLEMENT ME
    # TASK: CALCULATE POSITIONS
    # Update self.currOdoPos and self.prevOdoPos
    self.prevOdoPos = self.currOdoPos

    (com_dist,dtheta) = self.getPoseChange()
    curr_theta = self.prevOdoPos[2]
    dx = com_dist*cos(curr_theta)
    dy = com_dist*sin(curr_theta)

    self.currOdoPos = (self.currOdoPos[0] + dx,self.currOdoPos[1] + dy,self.currOdoPos[2] + dtheta)



  def getOdoPosition(self):
    # IMPLEMENT ME
    # TASK: RETURNS POSITION TUPLE
    # Return a tuple of (x [mm], y [mm], theta [rad])
    return self.currOdoPos # [mm], [rad], [s]

  def publishOdometry(self):
    # IMPLEMENT ME
    # TASK: PUBLISHES BOT_ODO_POSE MESSAGE
    msg = odo_pose_xyt_t()
    self.calcOdoPosition()
    msg.xyt = self.currOdoPos
    msg.utime = time.time()
    #print "Odometry Message"
    #print msg.xyt
    #print self.currOdoPos
    #print "--------------"
    self.lc.publish("BOT_ODO_POSE", msg.encode())

  def publishVelocities(self):
    # IMPLEMENT ME
    # TASK: PUBLISHES BOT_ODO_VEL MESSAGE
    msg = odo_dxdtheta_t()
    self.calcVelocities()
    msg.dt = time.time() - self.prevVelMesTime
    self.prevVelMesTime = time.time()
    (msg.dxy,msg.dtheta,xxx) = self.getVelocities()
    msg.utime = time.time()
    #print "Velocity Message"
    #print (msg.dxy,msg.dtheta)
    #print "--------------"
    self.lc.publish("BOT_ODO_VEL", msg.encode())

  def motorFeedbackHandler(self,channel,data):
    self.lcm_msg_counter[0] = self.lcm_msg_counter[0] + 1
    msg = maebot_motor_feedback_t.decode(data)
    self.currEncPos = (msg.encoder_left_ticks, msg.encoder_right_ticks, 0.0)
    time_now = time.time()

    self.prevReadTime = time_now
    if ( self.prevEncPos == self.currEncPos):
    	if (self.stopped):
    		self.timeSinceStopped = time_now-self.stopTime
    	else:
    		self.stopped = True
    		self.stopTime = time_now
    else:
    	self.stopped = False
    	self.timeSinceStopped = -1
    	self.stopTime = -1
    	self.gyroHistory = []
    if (not self.encReady):
        self.prevEncPos = self.currEncPos
        self.encReady = True
  
  def sensorDataHandler(self,channel,data):
    self.lcm_msg_counter[1] = self.lcm_msg_counter[1] + 1
    msg = maebot_sensor_data_t.decode(data)
    self.currGyroYaw = pi*(msg.gyro_int[2]/(1000000*self.gyroSensitivity))/180.0
    #print("Current Yaw - "+str(self.currGyroYaw))
    #print("Time since stopped = "+ str(self.timeSinceStopped)+" \t Gyro Bias = "+str(self.gyroBias))
    if(not self.gotOneGyro):
	self.gotOneGyro = True
	self.prevGyroYaw = self.currGyroYaw
	return
    if(not self.gyroReady):
    	self.gyroHistory.append(self.currGyroYaw- self.prevGyroYaw)
    	hist_len = len(self.gyroHistory)
    	if (self.timeSinceStopped > 1.0):
    	    self.gyroBias = sum(self.gyroHistory)/hist_len
    if (self.gyroBias!=-999):
    	self.gyroReady = True

    self.currGyroDtheta = self.currGyroYaw - self.prevGyroYaw - self.gyroBias
    self.prevGyroYaw = self.currGyroYaw

  def MainLoop(self):
    oldTime = time.time()
    frequency = 20;
    while(1):
      self.lc.handle()
      if(self.lcm_msg_counter[0] * self.lcm_msg_counter[1] > 0):
      #if(time.time()-oldTime > 1.0/frequency):
	#print "DO SOMETHING.."
	#print self.encReady
	#print self.gyroReady
        if self.encReady and self.gyroReady:
            self.publishOdometry()
            self.publishVelocities()
            self.prevEncPos = self.currEncPos
	self.lcm_msg_counter[0] = 0
        self.lcm_msg_counter[1] = 0
        oldTime = time.time()


if __name__ == "__main__":

  robot = Maebot()
  robot.MainLoop()
