import sys, os, time
import lcm
import math
from lcmtypes import rplidar_laser_t

class LocalController:
    def __init__(self,ctrl_rate=2,care_rad=200):
        self.lidar_r = []
	self.lidar_theta = []

        self.goal_force = 1
        self.rel_waypoint = 0

        self.force = 0.0
        self.ctrl_rate = ctrl_rate# In Hertz
        self.care_rad = care_rad        # Radius (in m) that the robot cares about

        self.lc = lcm.LCM()
        lidarSub = self.lc.subscribe("RPLIDAR_LASER", self.laserHandler)
        self.last_ctrl_time = time.time()
        self.ctrlT = 1/ctrl_rate

    # Input a complex number representing the goal direction
    def setWaypoint(self,g):
        self.rel_waypoint = g

    def laserHandler(self,channel,data):
	msg = rplidar_laser_t.decode(data)
	num_ranges = msg.nranges
	self.lidar_r = msg.ranges
	self.lidar_theta = msg.thetas

    # Assumes waypoint in x,y
    def computeForce(self):
       forces = [exp(t*1j)*(1/r*r)) for (r,t) in zip(self.lidar_r,self.lidar_theta) if r<self.care_rad]
       self.force = sum(forces) + self.goal_force*self.rel_waypoint/(abs(self.rel_waypoint))

    def Main(self):
        while(1):
            self.lc.handle()
            if( time.time() - self.last_ctrl_time > self.ctrlT):
                self.computeForce()
                self.last_ctrl_time = time.time()



