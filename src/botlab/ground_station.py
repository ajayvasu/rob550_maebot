import sys, os, time
import lcm
import math
import csv

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import matplotlib.backends.backend_agg as agg
import numpy as np
import pygame
from pygame.locals import *

from lcmtypes import maebot_diff_drive_t
from lcmtypes import velocity_cmd_t
from lcmtypes import pid_init_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import rplidar_laser_t

from breezyslam.algorithms import Deterministic_SLAM, RMHC_SLAM
from breezyslam.components import Laser

from scipy import signal
from maps import DataMatrix
from slam import Slam
import cmath
import math
from PIL import Image
from rrt_new import ConnectRRT
import pdb

# Map size, scale
MAP_SIZE_PIXELS          = 1000
MAP_SIZE_METERS          =  10
MAX_DEPTH		 = 5
USE_ODOMETRY		 = True
MAP_QUALITY		 = 7
HOLE_WIDTH_MM		 = 75

odo_path =[] 
slam_path = []

default_kp = 0.0072
default_ki = 0.00026
default_kd = 0.00027
default_isat = 10000

ctrl_rate = 0.5 #Hz
slam_rate = 1 #Hz

lin_motor_sp = 0.4
lin_turn_sp = 0.35

def adaptive_threshold(image,size,thresh_ad):
    block = np.ones((size, size))/(size*size)
    mean_conv = signal.convolve2d(image, block, mode='same', boundary='symm')
    return np.where(mean_conv<thresh_ad,0,255)
def norm(v):
    sqsum = 0.0
    for i in v:
        sqsum = sqsum + i*i
    return math.sqrt(sqsum)

def normalize_ang(a):
    return 180*math.atan2(math.sin(a),math.cos(a))/math.pi


class MainClass:
    def __init__(self, width=640, height=480, FPS=10):
        pygame.init()
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((self.width, self.height))
	self.surf = []
        #LOCAL CTRL
        self.rel_waypoint = 1+0*1j
	self.PIDturn = False
	self.PIDforward = False
	self.PIDtimeout = 3.0
	self.PIDprevtime = time.time()

	    # SLAM
	    # Create a CoreSLAM object with laser params and optional robot object
	self.laser_model = Laser(360,8,360,1000)
        self.slam = Slam([],self.laser_model, 8.0, 100)
        self.last_slam_time = time.time()
        self.slamT = 1/slam_rate
        self.lc = lcm.LCM()
        odoPoseSub = self.lc.subscribe("BOT_ODO_POSE", self.odoPoseHandler)
        odoVelocitySub = self.lc.subscribe("BOT_ODO_VEL", self.odoVelHandler)
        lidarSub = self.lc.subscribe("RPLIDAR_LASER", self.laserHandler)

        # Prepare Figure for Lidar
        self.fig = plt.figure(figsize=[3, 3], # Inches
                              dpi=100)    # 100 dots per inch,
        self.fig.patch.set_facecolor('white')
        self.fig.add_axes([0,0,1,1],projection='polar')
        self.ax = self.fig.gca()

	self.auto_mode = False
        self.poseUpdated = False
        self.velUpdated = False
        self.map = bytearray(MAP_SIZE_PIXELS*MAP_SIZE_PIXELS)
        self.data_mat = DataMatrix()
	self.lidar_x = []
	self.lidar_y = []
	self.lidar_r = []
	self.lidar_theta = []
	self.lidar_first = True
        self.goal_point = []
	self.slampose = []

	self.odoPose = []
	self.map_surf = []
	self.turning = False
        # Alert
        self.alert_theta = 0.45
	self.min_r = 0.14

	self.plan = []
        self.last_forward_time = time.time()
        self.last_turn_time = time.time()
	self.turn_timeout = 2.0
	self.forward_timeout = 4.0
        # Tolerance for path deviation
        self.tolerance = 0.0 
        # Prepare Figures for control
        path = os.path.realpath(__file__)
        path = path[:-17] + "maebotGUI/"

        self.arrowup = pygame.image.load(path + 'fwd_button.png')
        self.arrowdown = pygame.image.load(path + 'rev_button.png')
        self.arrowleft = pygame.image.load(path + 'left_button.png')
        self.arrowright = pygame.image.load(path + 'right_button.png')
        self.resetbut = pygame.image.load(path + 'reset_button.png')
        self.arrows = [0,0,0,0]
        # PID Initialization - Change for your Gains!
        command = pid_init_t()
        command.kp = default_kp
        command.ki = default_ki
        command.kd = default_kd
        command.iSat = default_isat # Saturation of Integral Term.
	self.turn_timeout = 3.0
	self.forward_timeout = 7.0
                                    # If Zero shoudl reset the Integral Term
        self.lc.publish("GS_PID_INIT",command.encode())

    def laserHandler(self,channel,data):
	msg = rplidar_laser_t.decode(data)
	num_ranges = msg.nranges
	self.lidar_r = msg.ranges
	self.lidar_theta = msg.thetas

	self.last_slam_time = time.time()
        degrees = [int(x*180/math.pi) for x in self.lidar_theta]
	points =  [(r*1000,t) for (r,t) in zip(self.lidar_r, degrees)]
        # Do Slam
        if self.velUpdated:
	    slam_pose = self.slam.updateSlam(points, self.odoVel)
            self.slampose = slam_pose
	    if(self.odoPose):
		print "----------------------"
	    	print self.slampose
	    	slam_path.append(self.slampose[0])
	    	slam_path.append(self.slampose[1])
	    	print self.odoPose
	    	odo_path.append(self.odoPose[0])
	    	odo_path.append(self.odoPose[1])
	    	print "----------------------"
	    self.data_mat.drawBreezyMap(self.slam.getBreezyMap())
	if self.poseUpdated:
            if self.lidar_first:
                self.data_mat.getRobotPos(slam_pose,True)
                self.lidar_first = False
            else:
                self.data_mat.getRobotPos(slam_pose)
	plt.cla()


        self.ax.plot(self.lidar_theta,self.lidar_r,'or',markersize=2)

        self.ax.plot(1,3,'-g',markersize=10)
        self.ax.set_rmax(1)
        self.ax.set_theta_direction(-1)
        self.ax.set_theta_zero_location("N")
        self.ax.set_thetagrids([0,45,90,135,180,225,270,315],
                                labels=['','','','','','','',''],
                                frac=None,fmt=None)
        self.ax.set_rgrids([0.5,1.0,1.5],labels=['0.5','1.0',''],
                                angle=None,fmt=None)
        canvas = agg.FigureCanvasAgg(self.fig)
        canvas.draw()
        renderer = canvas.get_renderer()
        raw_data = renderer.tostring_rgb()
        size = canvas.get_width_height()
        self.surf = pygame.image.fromstring(raw_data, size, "RGB")
        self.drawMap()
        # Set Alert if nearest point is too close
        alert_rs = [r for (r,t) in zip(self.lidar_r,self.lidar_theta) if t<self.alert_theta or t>2*math.pi-self.alert_theta]
        if len(alert_rs)>0 and min(alert_rs) <self.min_r and not self.turning:
	    print "Panic! Min dist - {0}".format(min(alert_rs))
            self.panic = True
	    self.plan = [] # Force control to "restart"
	if self.auto_mode:
	    self.control()


    def drawMap(self):
	if(len(self.data_mat.robot_pix) == 0):
	    return
	self.data_mat.drawInset()
        #robot_pix = self.data_mat.robot_pix
        mapSize_pix = self.data_mat.insetSize_pix
        mapMatrix = self.data_mat.insetMatrix

        robot = np.zeros((mapMatrix.shape[0], mapMatrix.shape[1]), dtype=bool) # initialize data matrix

        #self.data_mat.drawRobot(robot, robot_pix, 1)
        #self.data_mat.drawPath(robot, slice(None,-1,None), 1) # draw all values except last one # same as ":-1"

        GB = np.where(robot, 0, mapMatrix).astype(np.uint8) # copy map, invert values, assign to color layers
        R = np.where(robot, 255, GB).astype(np.uint8) # add robot path to red layer

        im = Image.fromarray(np.dstack((R,GB,GB))) # create image from depth stack of three layers

        mode = im.mode
        size = im.size
        data = im.tostring()
        assert mode in "RGB", "RGBA"
        self.map_surf = pygame.image.fromstring(data, size, mode)

    def odoPoseHandler(self,channel,data):
        msg = odo_pose_xyt_t.decode(data)
        #print "POSE HNADLER CALLED"
        self.odoPose = tuple(msg.xyt)
        self.poseUpdated = True

    def odoVelHandler(self,channel,data):
        msg = odo_dxdtheta_t.decode(data)
        self.odoVel = (msg.dxy,msg.dtheta,msg.dt)
        self.velUpdated = True

    def commandPIDTurn(self,theta):
        # Turn in place
	print "Turn {0} degrees".format(theta)
        if theta > 0:
            ang_sign = 1
        else:
            ang_sign = -1
        command = velocity_cmd_t()
        command.FwdSpeed = 0.0
        command.Distance = 0.0
        command.AngSpeed = ang_sign*40.0
        command.Angle = theta
        self.last_turn_time = time.time()
        self.forward = False
        self.turning = True
	self.panic = False
        self.lc.publish("GS_VELOCITY_CMD",command.encode())

    def commandPIDForward(self,dist):
        # Go Forward
	print "Move Forward {0}".format(dist)
	print "Robot at {0} going to {1}". format(self.data_mat.robot_pix,self.next_goal)
        command = velocity_cmd_t()
        command.FwdSpeed = 90.0
        command.Distance = dist/self.data_mat.mm2pix
        command.AngSpeed =0.0
        command.Angle =0.0
        self.last_forward_time = time.time()
        self.forward = True
        self.turning = False
        self.lc.publish("GS_VELOCITY_CMD",command.encode())

    def commandPIDReset(self):
        command = velocity_cmd_t()
        command.FwdSpeed = 0.0
        command.Distance = 0.0
        command.AngSpeed = 0.0
        command.Angle = 0.0
        self.lc.publish("GS_VELOCITY_CMD",command.encode())

    def getWayPoints(self):
        #TODO: Keep running RRT until certain conditions for the next point are true?
	# Get maebot xyt
        rx,ry,rt = self.data_mat.robot_pix
        goalPoint = self.goal_point
	startPoint = (rx,ry)
	pixdist2goal = norm((goalPoint[0] - rx,goalPoint[1] - ry))
	print "Dist to goal(pix) = ".format(pixdist2goal)
	if( pixdist2goal < 10):
	    self.commandPIDReset()
	    print "REACHED!!!"
	    self.auto_mode = False
	#num_cols = self.data_mat.mapMatrix.shape[1]
	#rrt_st = (ry,num_cols-rx)
	#rrt_goal = (self.goal_point[1],num_cols- self.goal_point[0])
        rrt_st = (ry,rx)
	rrt_goal = (self.goal_point[1],self.goal_point[0])
	#crrt = ConnectRRT(self.data_mat.mapMatrix,startPoint,goalPoint)
        crrt = ConnectRRT(self.img2rrt,rrt_st,rrt_goal)
	self.plan = crrt.plan()
	if not self.plan:
	    print "RRT Plan failed..."
	    self.getWayPoints()
	    return
	self.plan = [(y,x) for (x,y) in self.plan]
        # The first element of the plan is the current node
	self.start = self.plan[0]
        self.next_goal = self.plan[1]
        # Get relative disp and angle of next goal from
        # current pose
	tx = int(self.next_goal[0] - rx)
	ty = int(self.next_goal[1] - ry)
        self.next_dist  = norm((tx,ty))
        self.next_theta = normalize_ang(math.atan2(tx,ty) + rt*math.pi/180)

	# Print Current Pose
	print "Robot at"
	print (rx,ry,rt)
	# Print Rel Goal
	print "Relative Goal"
	print (tx,ty)
	# Print RRT command
	print "RRT Next point"
        print self.plan
	print (self.next_dist,self.next_theta)

        # Set Current time
        self.rrt_command_time = time.time()
	self.last_turn_time = time.time()
	self.commandPIDTurn(self.next_theta)
        self.turning = True
        self.forward = False

   # Gets called everytime a laser scan is obtained and the robot pose
   # is updated by slam
    def control(self):
        # Uncomment to debug
        # pdb.set_trace()
        # If we don't have a plan, make one
        if (not self.plan):
            self.commandPIDReset()
            self.getWayPoints()
        if (self.turning):
            # Has turn timedout? Time to start moving
            if(time.time() - self.last_turn_time > self.turn_timeout):
                self.commandPIDForward(self.next_dist)
        if(self.forward):
            # Has turn timedout? Restart Plan
            if(time.time() - self.last_forward_time > self.forward_timeout):
                self.plan = []
                self.control()
                return
            # Calculate deviation from path
            rx,ry,rt = self.data_mat.robot_pix
            actual_pos = (rx-self.start[0], ry-self.start[1])
            segment = (self.next_goal[0] - self.start[0], self.next_goal[1] - self.start[1])
            n_seg = norm(segment)

            unit_vec = (segment[0]/n_seg,segment[1]/n_seg)
            dot_prod = unit_vec[0]*actual_pos[0] + unit_vec[1]*actual_pos[1]
            ideal_pos = (self.start[0]+dot_prod*unit_vec[0],self.start[1]+dot_prod*unit_vec[1])

            vec2goal = (self.next_goal[0] - rx,self.next_goal[1] - ry)
            if(norm(vec2goal)/n_seg < 0.05):
		print "Completed"
                self.plan = []
                self.control()
                return
            dx = ideal_pos[0] - rx
            dy = ideal_pos[1] - ry
            deviation = math.sqrt(dx*dx + dy*dy)
            if (deviation > self.tolerance):
		print "Deviated"
                self.plan = []
                self.control()
                return

    def MainLoop(self):
        pygame.key.set_repeat(1, 20)
        vScale = 0.5
	self.tolerance = 200*self.data_mat.mm2pix

        # Prepare Text to Be output on Screen
        font = pygame.font.SysFont("DejaVuSans Mono",14)

        while 1:
            leftVel = 0
            rightVel = 0
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
		    print "saving.."
		    res = []
		    res.append(slam_path)
		    res.append(odo_path)
		    with open("slam_odo", "w") as output:
    			writer = csv.writer(output, lineterminator='\n')
    			for val in res:
        		    writer.writerow([val]) 
                    sys.exit()
                    #self.data_mat.saveImage()
                elif event.type == KEYDOWN:
                    if ((event.key == K_ESCAPE) or (event.key == K_q)):
                        sys.exit()
                    key = pygame.key.get_pressed()
		    #print key
		    if (event.key == K_z):
			self.auto_mode = False
		  	print "Auto mode OFF"
		    elif (event.key == K_a):
			self.auto_mode = True
        		self.img2rrt = adaptive_threshold(self.data_mat.mapMatrix,size=5,thresh_ad = 250)
			print "Auto mode ON"
			#self.data_mat.saveImage()
			#Image.fromarray(np.uint8(self.img2rrt)).save('/home/student/team12/new_code/src/botlab/map_images/rrt_map.png')
		    elif (event.key == K_p):
			print self.data_mat.robot_pix
                    elif key[K_RIGHT]:
                        leftVel = leftVel + lin_turn_sp
                        rightVel = rightVel - lin_turn_sp
                    elif key[K_LEFT]:
                        leftVel = leftVel - lin_turn_sp
                        rightVel = rightVel + lin_turn_sp
                    elif key[K_UP]:
                        leftVel = leftVel + lin_motor_sp
                        rightVel = rightVel + lin_motor_sp
                    elif key[K_DOWN]:
                        leftVel = leftVel - lin_motor_sp
                        rightVel = rightVel - lin_motor_sp
                    #elif key[K_m]:
                        #self.data_mat.saveImage()
                    elif key[K_g]:
			self.goal_point = (self.data_mat.robot_pix[0], self.data_mat.robot_pix[1])
			print "Goal Point SET!!"
			print self.goal_point
                    else:
			print "RESET"
                        leftVel = 0.0
                        rightVel = 0.0
                    cmd = maebot_diff_drive_t()
                    cmd.motor_right_speed = vScale * rightVel
                    cmd.motor_left_speed = vScale * leftVel
                    self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    command = velocity_cmd_t()
                    command.Distance = 987654321.0
                    if event.button == 1:
                        if ((event.pos[0] > 438) and (event.pos[0] < 510) and
                            (event.pos[1] > 325) and (event.pos[1] < 397)):
                            command.FwdSpeed = 80.0
                            command.Distance = 1000.0
                            command.AngSpeed = 0.0
                            command.Angle = 0.0
                            print "Commanded PID Forward One Meter!"
                        elif ((event.pos[0] > 438) and (event.pos[0] < 510) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = -80.0
                            command.Distance = -1000.0
                            command.AngSpeed = 0.0
                            command.Angle = 0.0
                            print "Commanded PID Backward One Meter!"
                        elif ((event.pos[0] > 363) and (event.pos[0] < 435) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = 0.0
                            command.Distance = 0.0
                            command.AngSpeed = 35.0
                            command.Angle = 90.0
                            print "Commanded PID Left One Meter!"
                        elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = 0.0
                            command.Distance = 0.0
                            command.AngSpeed = -35.0
                            command.Angle = -90.0
                            print "Commanded PID Right One Meter!"
                        elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 325) and (event.pos[1] < 397)):
                            pid_cmd = pid_init_t()
                            pid_cmd.kp = default_kp        # CHANGE FOR YOUR GAINS!
                            pid_cmd.ki = default_ki        # See initialization
                            pid_cmd.kd = default_kd
                            pid_cmd.iSat = default_isat
                            command.FwdSpeed = 0.0
                            command.Distance = 0.0
                            command.AngSpeed = 0.0
                            command.Angle = 0.0
                            self.lc.publish("GS_PID_INIT",pid_cmd.encode())
                            print "Commanded PID Reset!"
                        if (command.Distance != 987654321.0):
                            self.lc.publish("GS_VELOCITY_CMD",command.encode())

            self.screen.fill((255,255,255))


            # Plot Lidar Scans
            # IMPLEMENT ME - CURRENTLY ONLY PLOTS ONE DOT
            if self.surf:
                self.screen.blit(self.surf, (320,0))
	    if self.map_surf:
        	self.screen.blit(self.map_surf, (20,20))
            # Position and Velocity Feedback Text on Screen
            self.lc.handle()

            # Update text boxes
            if(self.poseUpdated):
                pygame.draw.rect(self.screen,(0,0,0),(5,350,300,120),2)
                text = font.render("  POSITION  ",True,(0,0,0))
                self.screen.blit(text,(10,360))
                text = font.render("x: "+str(self.odoPose[0]),True,(0,0,0))
                self.screen.blit(text,(10,390))
                text = font.render("y: "+str(self.odoPose[1]),True,(0,0,0))
                self.screen.blit(text,(10,420))
                text = font.render("t: "+str(self.odoPose[2]/math.pi*180.0),True,(0,0,0))
                self.screen.blit(text,(10,450))
            else:
                pygame.draw.rect(self.screen,(0,0,0),(5,350,300,120),2)
                text = font.render("    POSITION    ",True,(0,0,0))
                self.screen.blit(text,(10,360))
                text = font.render("x: 9999 [mm]",True,(0,0,0))
                self.screen.blit(text,(10,390))
                text = font.render("y: 9999 [mm]",True,(0,0,0))
                self.screen.blit(text,(10,420))
                text = font.render("t: 9999 [deg]",True,(0,0,0))
                self.screen.blit(text,(10,450))

            if(self.velUpdated):
                text = font.render("  VELOCITY  ",True,(0,0,0))
                self.screen.blit(text,(150,360))
                text = font.render("dxy/dt:  "+str(self.odoVel[0]/self.odoVel[2])+"[mm/s]",True,(0,0,0))
                self.screen.blit(text,(150,390))
                text = font.render("dth/dt: "+str(self.odoVel[1]/self.odoVel[2])+"[deg/s]",True,(0,0,0))
                self.screen.blit(text,(150,420))
                text = font.render("dt: "+str(self.odoVel[2])+"[s]",True,(0,0,0))
                self.screen.blit(text,(150,450))
            else:
                text = font.render("    VELOCITY    ",True,(0,0,0))
                self.screen.blit(text,(150,360))
                text = font.render("dxy/dt: 9999 [mm/s]",True,(0,0,0))
                self.screen.blit(text,(150,390))
                text = font.render("dth/dt: 9999 [deg/s]",True,(0,0,0))
                self.screen.blit(text,(150,420))
                text = font.render("dt:     9999 [s]",True,(0,0,0))
                self.screen.blit(text,(150,450))


            # Plot Buttons
            self.screen.blit(self.arrowup,(438,325))
            self.screen.blit(self.arrowdown,(438,400))
            self.screen.blit(self.arrowleft,(363,400))
            self.screen.blit(self.arrowright,(513,400))
            self.screen.blit(self.resetbut,(513,325))

            pygame.display.flip()


MainWindow = MainClass()
MainWindow.MainLoop()
