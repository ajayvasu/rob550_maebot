import sys, os, time
import lcm
import math

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import matplotlib.backends.backend_agg as agg
import numpy as np
import pygame
from pygame.locals import *

from lcmtypes import maebot_diff_drive_t
from lcmtypes import velocity_cmd_t
from lcmtypes import pid_init_t
from lcmtypes import odo_dxdtheta_t
from lcmtypes import odo_pose_xyt_t
from lcmtypes import rplidar_laser_t

from breezyslam.algorithms import Deterministic_SLAM, RMHC_SLAM
from breezyslam.components import Laser

from maps import DataMatrix
from slam import Slam
import cmath
import math
from PIL import Image
from rrt_new import ConnectRRT
import pdb

# Map size, scale
MAP_SIZE_PIXELS          = 800
MAP_SIZE_METERS          =  8
MAX_DEPTH		 = 5
USE_ODOMETRY		 = True
MAP_QUALITY		 = 7
HOLE_WIDTH_MM		 = 75


default_kp = 0.0072
default_ki = 0.00026
default_kd = 0.0002
default_isat = 10000

ctrl_rate = 0.5 #Hz
slam_rate = 1 #Hz

lin_motor_sp = 0.4
lin_turn_sp = 0.35

def norm(v):
    sqsum = 0.0
    for i in v:
        sqsum = sqsum + i*i
    return math.sqrt(sqsum)


class MainClass:
    def __init__(self, width=640, height=480, FPS=10):
        pygame.init()
        self.width = width
        self.height = height
        self.screen = pygame.display.set_mode((self.width, self.height))
	self.surf = []
        #LOCAL CTRL
        self.rel_waypoint = 1+0*1j
	self.PIDturn = False
	self.PIDforward = False
	self.PIDtimeout = 3.0
	self.PIDprevtime = time.time()

	    # SLAM
	    # Create a CoreSLAM object with laser params and optional robot object
	self.laser_model = Laser(360,8,360,1000)
        self.slam = Slam([],self.laser_model, 8.0, 100)
        self.last_slam_time = time.time()
        self.slamT = 1/slam_rate
        self.lc = lcm.LCM()
        odoPoseSub = self.lc.subscribe("BOT_ODO_POSE", self.odoPoseHandler)
        odoVelocitySub = self.lc.subscribe("BOT_ODO_VEL", self.odoVelHandler)
        lidarSub = self.lc.subscribe("RPLIDAR_LASER", self.laserHandler)

        # Prepare Figure for Lidar
        self.fig = plt.figure(figsize=[3, 3], # Inches
                              dpi=100)    # 100 dots per inch,
        self.fig.patch.set_facecolor('white')
        self.fig.add_axes([0,0,1,1],projection='polar')
        self.ax = self.fig.gca()

	self.auto_mode = False
        self.poseUpdated = False
        self.velUpdated = False
        self.map = bytearray(MAP_SIZE_PIXELS*MAP_SIZE_PIXELS)
        self.data_mat = DataMatrix()
	self.lidar_x = []
	self.lidar_y = []
	self.lidar_r = []
	self.lidar_theta = []
	self.lidar_first = True
        self.goal_point = []
	self.slampose = []

	self.map_surf = []

        # Alert
        self.alert_theta = 1.3

        # Tolerance for path deviation
        self.tolerance = 0.2
        # Prepare Figures for control
        path = os.path.realpath(__file__)
        path = path[:-23] + "maebotGUI/"

        self.arrowup = pygame.image.load(path + 'fwd_button.png')
        self.arrowdown = pygame.image.load(path + 'rev_button.png')
        self.arrowleft = pygame.image.load(path + 'left_button.png')
        self.arrowright = pygame.image.load(path + 'right_button.png')
        self.resetbut = pygame.image.load(path + 'reset_button.png')
        self.arrows = [0,0,0,0]
        # PID Initialization - Change for your Gains!
        command = pid_init_t()
        command.kp = default_kp
        command.ki = default_ki
        command.kd = default_kd
        command.iSat = default_isat # Saturation of Integral Term.
                                    # If Zero shoudl reset the Integral Term
        self.lc.publish("GS_PID_INIT",command.encode())

    def laserHandler(self,channel,data):
	msg = rplidar_laser_t.decode(data)
	num_ranges = msg.nranges
	self.lidar_r = msg.ranges
	self.lidar_theta = msg.thetas

	self.last_slam_time = time.time()
        degrees = [int(x*180/math.pi) for x in self.lidar_theta]
	points =  [(r*1000,t) for (r,t) in zip(self.lidar_r, degrees)]
        # Do Slam
        if self.velUpdated:
	    slam_pose = self.slam.updateSlam(points, self.odoVel)
            self.slampose = slam_pose
	    self.data_mat.drawBreezyMap(self.slam.getBreezyMap())
	if self.poseUpdated:
            if self.lidar_first:
                self.data_mat.getRobotPos(slam_pose,True)
                self.lidar_first = False
            else:
                self.data_mat.getRobotPos(slam_pose)
	plt.cla()


        self.ax.plot(self.lidar_theta,self.lidar_r,'or',markersize=2)

        self.ax.plot(1,3,'-g',markersize=10)
        self.ax.set_rmax(1)
        self.ax.set_theta_direction(-1)
        self.ax.set_theta_zero_location("N")
        self.ax.set_thetagrids([0,45,90,135,180,225,270,315],
                                labels=['','','','','','','',''],
                                frac=None,fmt=None)
        self.ax.set_rgrids([0.5,1.0,1.5],labels=['0.5','1.0',''],
                                angle=None,fmt=None)
        canvas = agg.FigureCanvasAgg(self.fig)
        canvas.draw()
        renderer = canvas.get_renderer()
        raw_data = renderer.tostring_rgb()
        size = canvas.get_width_height()
        self.surf = pygame.image.fromstring(raw_data, size, "RGB")
        self.drawMap()
        # Set Alert if nearest point is too close
        alert_rs = [r for (r,t) in zip(self.lidar_r,self.lidar_theta) if t<self.alert_theta or t>2*math.pi-self.alert_theta]
        if min(alert_rs) <self.min_r:
            print "Panic"
            self.panic = True
	    self.plan = [] # Force control to "restart"
        self.control()


    def drawMap(self):
	if(len(self.data_mat.robot_pix) == 0):
	    return
	self.data_mat.drawInset()
	print self.data_mat.robot_pix
        #robot_pix = self.data_mat.robot_pix
        mapSize_pix = self.data_mat.insetSize_pix
        mapMatrix = self.data_mat.insetMatrix

        robot = np.zeros((mapMatrix.shape[0], mapMatrix.shape[1]), dtype=bool) # initialize data matrix

        #self.data_mat.drawRobot(robot, robot_pix, 1)
        #self.data_mat.drawPath(robot, slice(None,-1,None), 1) # draw all values except last one # same as ":-1"

        GB = np.where(robot, 0, mapMatrix).astype(np.uint8) # copy map, invert values, assign to color layers
        R = np.where(robot, 255, GB).astype(np.uint8) # add robot path to red layer

        im = Image.fromarray(np.dstack((R,GB,GB))) # create image from depth stack of three layers

        mode = im.mode
        size = im.size
        data = im.tostring()
        assert mode in "RGB", "RGBA"
        self.map_surf = pygame.image.fromstring(data, size, mode)

    def odoPoseHandler(self,channel,data):
        msg = odo_pose_xyt_t.decode(data)
        #print "POSE HNADLER CALLED"
        self.odoPose = tuple(msg.xyt)
        self.poseUpdated = True

    def odoVelHandler(self,channel,data):
        msg = odo_dxdtheta_t.decode(data)
        self.odoVel = (msg.dxy,msg.dtheta,msg.dt)
        self.velUpdated = True

    def commandPIDTurn(self,theta):
        # Turn in place
        if self.theta > 0:
            ang_sign = 1
        else:
            ang_sign = -1
        command = velocity_cmd_t()
        command.FwdSpeed = 0.0
        command.Distance = 0.0
        command.AngSpeed = ang_sign*35.0
        command.Angle = self.theta
        self.last_turn_time = time.time()
        self.forward = False
        self.turning = True
        self.lc.publish("GS_VELOCITY_CMD",command.encode())

    def commandPIDForward(self,dist):
        # Go Forward
        command = velocity_cmd_t()
        command.FwdSpeed = 90.0
        command.Distance = dist
        command.AngSpeed =0.0
        command.Angle =0.0
        self.last_turn_time = time.time()
        self.forward = True
        self.turning = False
        self.lc.publish("GS_VELOCITY_CMD",command.encode())
        # Has go forward timedout?
        if(time.time() - self.last_turn_time > turn_timeout):
            pass

    def commandPIDReset(self):
        command = velocity_cmd_t()
        command.FwdSpeed = 0.0
        command.Distance = 0.0
        command.AngSpeed = 0.0
        command.Angle = 0.0
        self.forward =False
        self.turning = False
        self.lc.publish("GS_VELOCITY_CMD",command.encode())

    def getWayPoints(self):
        #TODO: Keep running RRT until certain conditions for the next point are true?
	# Get maebot xyt
        rx,ry,rt = self.data_mat.robot_pix
        goalPoint = self.goal_point
	startPoint = (rx,ry)

        crrt = ConnectRRT(self.data_mat.mapMatrix,,goalPoint)
	self.plan = crrt.plan()
        # The first element of the plan is the current node
	self.start = plan[0]
        self.next_goal = plan[1]
        # Get relative disp and angle of next goal from
        # current pose
	tx = int(self.next_goal[0] - rx)
	ty = int(self.next_goal[1] - ry)
        self.next_dist  = norm((tx,ty))
        self.next_theta = math.atan2(ty,tx)*180.0/math.pi - rt
        # Print RRT command
        print (next_dist,next_theta)
        # Set Current time
        self.rrt_command_time = time.time()
        self.turning = True
        self.forward = False

   # Gets called everytime a laser scan is obtained and the robot pose
   # is updated by slam
    def control(self):
        # Uncomment to debug
        pdb.set_trace()
        # If we don't have a plan, make one
        if (not self.plan):
            self.getWayPoints()
            self.commandPIDReset()
        if (self.turning):
            # Has turn timedout? Time to start moving
            if(time.time() - self.last_turn_time > turn_timeout):
                self.commandPIDForward(self.next_dist)
        if(self.forward)
            # Has turn timedout? Restart Plan
            if(time.time() - self.forward > turn_timeout):
                self.plan = []
                self.control()
            # Calculate deviation from path
            rx,ry,rt = self.data_mat.robot_pix
            actual_pos = (rx-self.start[0], ry-self.start[1])
            dist_trav = norm(actual_pos)
            frac_dist = self.next_dist/dist_trav
            if (frac_dist > 0.95):
                self.plan = []
                self.control()
            ideal_pos_x = frac_dist*self.start[0] + (1-frac_dist)*self.next_goal[0]
            ideal_pos_y = frac_dist*self.start[1] + (1-frac_dist)*self.next_goal[1]
            dx = ideal_pos_x - rx
            dy = ideal_pos_y - ry
            deviation = math.sqrt(dx*dx + dy*dy)
            if (devaiation > self.tolerance):
                self.plan = []
                self.control()

    def MainLoop(self):
        pygame.key.set_repeat(1, 20)
        vScale = 0.5

        # Prepare Text to Be output on Screen
        font = pygame.font.SysFont("DejaVuSans Mono",14)

        while 1:
            leftVel = 0
            rightVel = 0
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.data_mat.saveImage()
                    sys.exit()
                elif event.type == KEYDOWN:
                    if ((event.key == K_ESCAPE) or (event.key == K_q)):
                        sys.exit()
                    key = pygame.key.get_pressed()
		    #print key
		    if (event.key == K_a):
			if self.auto_mode:
			    self.auto_mode = False
			else:
			    self.auto_mode = True
                    if key[K_RIGHT]:
                        leftVel = leftVel + lin_turn_sp
                        rightVel = rightVel - lin_turn_sp
                    elif key[K_LEFT]:
                        leftVel = leftVel - lin_turn_sp
                        rightVel = rightVel + lin_turn_sp
                    elif key[K_UP]:
                        leftVel = leftVel + lin_motor_sp
                        rightVel = rightVel + lin_motor_sp
                    elif key[K_DOWN]:
                        leftVel = leftVel - lin_motor_sp
                        rightVel = rightVel - lin_motor_sp
                    elif key[K_m]:
                        self.data_mat.saveImage()
                    elif key[K_g]:
			self.goal_point = (self.data_mat.robot_pix[0], self.data_mat.robot_pix[1])
			print self.goal_point
                        self.getWayPoints()

                    else:
			print "RESET"
                        leftVel = 0.0
                        rightVel = 0.0
                    cmd = maebot_diff_drive_t()
                    cmd.motor_right_speed = vScale * rightVel
                    cmd.motor_left_speed = vScale * leftVel
                    self.lc.publish("MAEBOT_DIFF_DRIVE",cmd.encode())
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    command = velocity_cmd_t()
                    command.Distance = 987654321.0
                    if event.button == 1:
                        if ((event.pos[0] > 438) and (event.pos[0] < 510) and
                            (event.pos[1] > 325) and (event.pos[1] < 397)):
                            command.FwdSpeed = 80.0
                            command.Distance = 1000.0
                            command.AngSpeed = 0.0
                            command.Angle = 0.0
                            print "Commanded PID Forward One Meter!"
                        elif ((event.pos[0] > 438) and (event.pos[0] < 510) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = -80.0
                            command.Distance = -1000.0
                            command.AngSpeed = 0.0
                            command.Angle = 0.0
                            print "Commanded PID Backward One Meter!"
                        elif ((event.pos[0] > 363) and (event.pos[0] < 435) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = 0.0
                            command.Distance = 0.0
                            command.AngSpeed = 35.0
                            command.Angle = 15.0
                            print "Commanded PID Left One Meter!"
                        elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 400) and (event.pos[1] < 472)):
                            command.FwdSpeed = 0.0
                            command.Distance = 0.0
                            command.AngSpeed = -35.0
                            command.Angle = -15.0
                            print "Commanded PID Right One Meter!"
                        elif ((event.pos[0] > 513) and (event.pos[0] < 585) and
                            (event.pos[1] > 325) and (event.pos[1] < 397)):
                            pid_cmd = pid_init_t()
                            pid_cmd.kp = default_kp        # CHANGE FOR YOUR GAINS!
                            pid_cmd.ki = default_ki        # See initialization
                            pid_cmd.kd = default_kd
                            pid_cmd.iSat = default_isat
                            command.FwdSpeed = 0.0
                            command.Distance = 0.0
                            command.AngSpeed = 0.0
                            command.Angle = 0.0
                            self.lc.publish("GS_PID_INIT",pid_cmd.encode())
                            print "Commanded PID Reset!"
                        if (command.Distance != 987654321.0):
                            self.lc.publish("GS_VELOCITY_CMD",command.encode())

            self.screen.fill((255,255,255))


            # Plot Lidar Scans
            # IMPLEMENT ME - CURRENTLY ONLY PLOTS ONE DOT
            if self.surf:
                self.screen.blit(self.surf, (320,0))
	    if self.map_surf:
        	self.screen.blit(self.map_surf, (20,20))
            # Position and Velocity Feedback Text on Screen
            self.lc.handle()

            # Update text boxes
            if(self.poseUpdated):
                pygame.draw.rect(self.screen,(0,0,0),(5,350,300,120),2)
                text = font.render("  POSITION  ",True,(0,0,0))
                self.screen.blit(text,(10,360))
                text = font.render("x: "+str(self.odoPose[0]),True,(0,0,0))
                self.screen.blit(text,(10,390))
                text = font.render("y: "+str(self.odoPose[1]),True,(0,0,0))
                self.screen.blit(text,(10,420))
                text = font.render("t: "+str(self.odoPose[2]),True,(0,0,0))
                self.screen.blit(text,(10,450))
            else:
                pygame.draw.rect(self.screen,(0,0,0),(5,350,300,120),2)
                text = font.render("    POSITION    ",True,(0,0,0))
                self.screen.blit(text,(10,360))
                text = font.render("x: 9999 [mm]",True,(0,0,0))
                self.screen.blit(text,(10,390))
                text = font.render("y: 9999 [mm]",True,(0,0,0))
                self.screen.blit(text,(10,420))
                text = font.render("t: 9999 [deg]",True,(0,0,0))
                self.screen.blit(text,(10,450))

            if(self.velUpdated):
                text = font.render("  VELOCITY  ",True,(0,0,0))
                self.screen.blit(text,(150,360))
                text = font.render("dxy/dt:  "+str(self.odoVel[0]/self.odoVel[2])+"[mm/s]",True,(0,0,0))
                self.screen.blit(text,(150,390))
                text = font.render("dth/dt: "+str(self.odoVel[1]/self.odoVel[2])+"[deg/s]",True,(0,0,0))
                self.screen.blit(text,(150,420))
                text = font.render("dt: "+str(self.odoVel[2])+"[s]",True,(0,0,0))
                self.screen.blit(text,(150,450))
            else:
                text = font.render("    VELOCITY    ",True,(0,0,0))
                self.screen.blit(text,(150,360))
                text = font.render("dxy/dt: 9999 [mm/s]",True,(0,0,0))
                self.screen.blit(text,(150,390))
                text = font.render("dth/dt: 9999 [deg/s]",True,(0,0,0))
                self.screen.blit(text,(150,420))
                text = font.render("dt:     9999 [s]",True,(0,0,0))
                self.screen.blit(text,(150,450))


            # Plot Buttons
            self.screen.blit(self.arrowup,(438,325))
            self.screen.blit(self.arrowdown,(438,400))
            self.screen.blit(self.arrowleft,(363,400))
            self.screen.blit(self.arrowright,(513,400))
            self.screen.blit(self.resetbut,(513,325))

            pygame.display.flip()


MainWindow = MainClass()
MainWindow.MainLoop()
